package org.homework3;

public class InitializeSchedule {
    public static String[][] initializeSchedule() {
        String[][] schedule = new String[7][2];

        schedule[0][0] = "Sunday";
        schedule[0][1] = "Go to courses; watch a movie.";
        schedule[1][0] = "Monday";
        schedule[1][1] = "Work on a project; go for a run.";
        schedule[2][0] = "Tuesday";
        schedule[2][1] = "Study for exams; attend a meeting.";
        schedule[3][0] = "Wednesday";
        schedule[3][1] = "Complete assignments; practice coding.";
        schedule[4][0] = "Thursday";
        schedule[4][1] = "Finish work tasks; relax with friends.";
        schedule[5][0] = "Friday";
        schedule[5][1] = "Enjoy the weekend; explore new places.";
        schedule[6][0] = "Saturday";
        schedule[6][1] = "Rest and recharge; spend time with family.";

        return schedule;
    }
}

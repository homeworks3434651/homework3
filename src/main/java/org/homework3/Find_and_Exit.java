package org.homework3;

public class Find_and_Exit {
    static int findDayIndex(String[][] schedule, String day) {
        for (int i = 0; i < schedule.length; i++) {
            if (schedule[i][0].equalsIgnoreCase(day)) {
                return i;
            }
        }
        return -1;
    }

    static void handleReturnOrExit(String input) {
        switch (input) {
            case "return":
                break;
            case "exit":
                System.out.println(ConsoleColors.PURPLE_BOLD_BRIGHT + "\nExiting the program. Goodbye!");
                System.exit(0);
                break;
        }
    }
}

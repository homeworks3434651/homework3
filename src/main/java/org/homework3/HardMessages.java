package org.homework3;

import java.util.Scanner;

import static org.homework3.ChangeTasks.changeTasks;
import static org.homework3.Find_and_Exit.findDayIndex;
import static org.homework3.RescheduleTasks.rescheduleTasks;

public class HardMessages {

    public static void processUserInputHard(String[][] schedule, String userInput, Scanner scanner) {
        if (userInput.startsWith("change") || userInput.startsWith("reschedule")) {
            String[] parts = userInput.split(" ", 2);

            if (parts.length < 2) {
                displayInvalidDayMessageHard();
                return;
            }

            String action = parts[0].toLowerCase();
            String day = parts[1];

            int index = findDayIndex(schedule, day);

            if (index != -1) {
                switch (action) {
                    case "change":
                        changeTasks(scanner, schedule, index);
                        break;
                    case "reschedule":
                        rescheduleTasks(scanner, schedule, index);
                        break;
                }
            } else {
                displayInvalidDayMessageHard();
            }
        } else {
            boolean dayFound = false;
            for (int i = 0; i < schedule.length; i++) {
                if (schedule[i][0].equalsIgnoreCase(userInput)) {
                    dayFound = true;
                    System.out.println(
                            ConsoleColors.BLUE_BACKGROUND
                                    + ConsoleColors.BLACK_BOLD_BRIGHT
                                    + "Your tasks for " + schedule[i][0] + ": " + schedule[i][1]
                                    + ConsoleColors.RESET);
                    break;
                }
            }

            if (!dayFound) {
                System.out.println(
                        ConsoleColors.RED_BACKGROUND_BRIGHT
                                + ConsoleColors.BLACK_BOLD_BRIGHT
                                + "Sorry, I don't understand you."
                                + ConsoleColors.RESET);
            }
        }
    }

    private static void displayInvalidDayMessageHard() {
        System.out.println(
                ConsoleColors.RED_BACKGROUND_BRIGHT
                        + ConsoleColors.BLACK_BOLD_BRIGHT
                        + "Invalid day."
                        + ConsoleColors.RESET);
    }
}

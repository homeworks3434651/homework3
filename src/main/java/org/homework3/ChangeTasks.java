package org.homework3;

import java.util.Scanner;

import static org.homework3.Find_and_Exit.handleReturnOrExit;

public class ChangeTasks {
    static void changeTasks(Scanner scanner, String[][] schedule, int index) {
        while (true) {
            System.out.println(ConsoleColors.BLUE_BRIGHT + "Please, input new tasks for " + schedule[index][0] +
                    " or enter 'return' to exit the change command or enter 'exit' to exit the program:");
            String newTasksInput = scanner.nextLine().trim();

            handleReturnOrExit(newTasksInput);

            switch (newTasksInput.toLowerCase()) {
                case "return":
                    return; // exit the change command
                case "exit":
                    System.out.println(ConsoleColors.PURPLE_BOLD_BRIGHT + "\nExiting the program. Goodbye!");
                    System.exit(0);
                    break;
                default:
                    // Check if the task already exists for the day
                    if (isTaskAlreadyExists(newTasksInput, schedule, index)) {
                        System.out.println(
                                ConsoleColors.RED_BRIGHT
                                        + "This task is already entered for this day. Please try again.");
                    } else {
                        schedule[index][1] = newTasksInput;
                        System.out.println(
                                ConsoleColors.GREEN_BACKGROUND
                                        + ConsoleColors.BLACK_BOLD_BRIGHT
                                        + "Tasks for " + schedule[index][0] + " have been updated."
                                        + ConsoleColors.RESET);
                        return; // exit the loop after successful entry
                    }
            }
        }
    }

    private static boolean isTaskAlreadyExists(String task, String[][] schedule, int index) {
        String existingTasks = schedule[index][1].toLowerCase().replaceAll("\\s+", "");
        String newTask = task.toLowerCase().replaceAll("\\s+", "");
        return existingTasks.contains(newTask);
    }
}

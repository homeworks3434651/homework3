package org.homework3;

import java.util.Scanner;

import static org.homework3.SimpleMessages.*;

public class ToDoListSimple {
    public static void playSimpleGame(String[][] schedule, Scanner scanner) {
        while (true) {
            System.out.print(ConsoleColors.YELLOW_BRIGHT + "\nEnter the day of the week or type 'exit': " + ConsoleColors.RESET);
            String userInput = scanner.nextLine().trim().toLowerCase();

            if (userInput.equals("exit")) {
                System.out.println(ConsoleColors.PURPLE_BOLD_BRIGHT + "\nExiting the program. Goodbye!");
                break;
            }

            if (!processUserInputSimple(schedule, userInput)) {
                displayInvalidDayMessageSimple();
            }
        }
    }
}

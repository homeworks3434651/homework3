package org.homework3;

import java.util.Scanner;

import static org.homework3.HardMessages.processUserInputHard;

public class ToDoListHard {
    public static void playHardGame(String[][] schedule, Scanner scanner) {
        while (true) {
            System.out.println(
                    ConsoleColors.YELLOW_BRIGHT
                            + "\nEnter the day of the week / 'change [day]' / 'reschedule [day]' / 'exit': "
                            + ConsoleColors.RESET);
            String userInput = scanner.nextLine().trim().toLowerCase();

            switch (userInput) {
                case "exit":
                    System.out.println(ConsoleColors.PURPLE_BOLD_BRIGHT + "\nExiting the program. Goodbye!");
                    System.exit(0);
                    break;
                default:
                    processUserInputHard(schedule, userInput, scanner);
                    break;
            }
        }
    }
}

package org.homework3;

import java.util.Scanner;

import static org.homework3.Find_and_Exit.*;

public class RescheduleTasks {
    static void rescheduleTasks(Scanner scanner, String[][] schedule, int index) {
        while (true) {
            System.out.println(
                    ConsoleColors.PURPLE_BRIGHT
                            + "Please, input the new day of the week for tasks scheduled on " + schedule[index][0]
                            + " or enter 'return' to exit the reschedule command \nor enter 'exit' to exit the program:");
            String newDay = scanner.nextLine().trim().toLowerCase();

            handleReturnOrExit(newDay);

            switch (newDay) {
                case "return":
                    return;
                case "exit":
                    System.out.println(ConsoleColors.PURPLE_BOLD_BRIGHT + "\nExiting the program. Goodbye!");
                    System.exit(0);
                    break;
                default:
                    // Check if the day is the same as the one used in a previous reschedule command
                    if (newDay.equalsIgnoreCase(schedule[index][0])) {
                        System.out.println(
                                ConsoleColors.RED_BRIGHT
                                        + "This day was already used in a previous reschedule command. Please try again.");
                    } else {
                        // Check if the day is valid
                        if (isValidDay(newDay)) {
                            int newDayIndex = findDayIndex(schedule, newDay);

                            if (newDayIndex != -1) {
                                schedule[newDayIndex][1] = schedule[index][1];
                                System.out.println(
                                        ConsoleColors.GREEN_BACKGROUND
                                                + ConsoleColors.BLACK_BOLD_BRIGHT
                                                + "Tasks scheduled on " + schedule[index][0]
                                                + " have been rescheduled to " + schedule[newDayIndex][0].toUpperCase().charAt(0)
                                                + schedule[newDayIndex][0].substring(1) + "."
                                                + ConsoleColors.RESET);
                                return; // exit the loop after successful entry
                            } else {
                                System.out.println("Unexpected error occurred during rescheduling tasks. Please try again or enter 'return' to exit the reschedule command or enter 'exit' to exit the program.");
                            }
                        } else {
                            System.out.println(ConsoleColors.RED_BOLD_BRIGHT + "Invalid day.");
                        }
                    }
            }
        }
    }

    private static boolean isDayAlreadyRescheduled(String day, String[][] schedule) {
        for (int i = 0; i < schedule.length; i++) {
            if (schedule[i][0].equalsIgnoreCase(day)) {
                return true;
            }
        }
        return false;
    }

    private static boolean isValidDay(String day) {
        String[] validDays = {"sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday"};
        for (String validDay : validDays) {
            if (validDay.equalsIgnoreCase(day)) {
                return true;
            }
        }
        return false;
    }
}

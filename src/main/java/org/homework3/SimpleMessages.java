package org.homework3;

public class SimpleMessages {
    public static boolean processUserInputSimple(String[][] schedule, String userInput) {
        for (int i = 0; i < schedule.length; i++) {
            if (schedule[i][0].equalsIgnoreCase(userInput)) {
                System.out.println(
                        ConsoleColors.BLUE_BACKGROUND
                                + ConsoleColors.BLACK_BOLD_BRIGHT
                                + "Your tasks for " + schedule[i][0] + ": " + schedule[i][1]
                                + ConsoleColors.RESET);
                return true;
            }
        }
        return false;
    }

    public static void displayInvalidDayMessageSimple() {
        System.out.println(
                ConsoleColors.RED_BACKGROUND_BRIGHT
                        + ConsoleColors.BLACK_BOLD_BRIGHT
                        + "Sorry, I don't understand you, please try again."
                        + ConsoleColors.RESET);
    }
}

package org.homework3;

import java.util.Scanner;

public class ToDoList_Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Let the to-do list game begin!");

        int userLevel;
        while (true) {
            System.out.print("Choose the level (Simple[1] or Hard[2]): ");
            String userInput = scanner.nextLine().trim();

            if (userInput.matches("\\d+")) {
                userLevel = Integer.parseInt(userInput);
                if (userLevel == 1 || userLevel == 2) {
                    break;
                } else {
                    System.out.println("Invalid level selection. Please enter 1 or 2.");
                }
            } else {
                System.out.println("Please enter a valid integer for the level.");
            }
        }

        String[][] schedule = InitializeSchedule.initializeSchedule();

        if (userLevel == 1) {
            ToDoListSimple.playSimpleGame(schedule, scanner);
        } else if (userLevel == 2) {
            ToDoListHard.playHardGame(schedule, scanner);
        }

        scanner.close();
    }
}
